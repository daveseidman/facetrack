import { dataURLToBlob } from './utils';
import { weeks } from './config';

const isLocal = window.location.href.indexOf('localhost') >= 0;

export default class Record {
  constructor(state) {
    this.state = state;
    this.recordframe = this.recordFrame.bind(this);
    this.startRecording = this.startRecording.bind(this);
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);

    this.element = document.createElement('div');
    this.element.className = 'record';

    this.outline = document.createElement('div');
    this.outline.className = 'record-outline hidden';

    this.button = document.createElement('div');
    this.button.className = 'record-button hidden';
    this.button.addEventListener('click', () => { state.recording = true; });

    this.recordRing = document.createElement('div');
    this.recordRing.className = 'record-button-ring';
    this.recordRing.innerHTML = '<svg width="100" height="100" viewBox="0 0 100 100"><circle id="percent" cx="50" cy="50" r="45" fill="none" stroke="white" stroke-width="4" stroke-linecap="round" stroke-dasharray="280" style="transition: all .5s" /></svg>';
    this.button.appendChild(this.recordRing);
    this.recordRingPath = this.recordRing.querySelector('#percent');

    this.element.appendChild(this.outline);
    this.element.appendChild(this.button);
  }

  show() {
    this.button.classList.remove('hidden');
  }

  hide() {
    this.button.classList.add('hidden');
  }

  recordFrame(timeout) {
    setTimeout(() => {
      this.state.week = timeout - 1;
    }, (timeout * 1000) - 500);

    setTimeout(() => {
      this.formData.append(`${this.hash}-${timeout}`, dataURLToBlob(this.canvas.toDataURL()));
      const percent = (1 - (timeout / weeks.length)) * 280;
      this.recordRingPath.setAttribute('stroke-dashoffset', percent);
    }, timeout * 1000);
  }


  startRecording() {
    this.recordRingPath.setAttribute('stroke-dashoffset', 280);

    this.outline.classList.remove('hidden');
    this.hash = Math.random().toString(36).substring(7);
    this.formData = new FormData();

    // TODO: refine this and add a countdown:
    for (let i = 1; i <= weeks.length; i += 1) {
      this.recordFrame(i);
    }

    setTimeout(() => {
      const url = `${isLocal ? 'facetrack/' : ''}sendImage/${this.hash}`;
      console.log('url', url, isLocal);
      this.outline.classList.add('hidden');
      fetch(url, {
        method: 'POST',
        body: this.formData,
      }).then(res => res.json()).then((res) => {
        console.log('gif is ready', res);
        this.state.recording = false;
        this.state.previewing = this.hash;
        this.state.week = 0;
      });
    }, weeks.length * 1000);
  }
}
