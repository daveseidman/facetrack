import { weeks, types } from './config';

export default class Overlays {
  constructor(state) {
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.changeWeek = this.changeWeek.bind(this);
    this.changeTypes = this.changeTypes.bind(this);

    this.state = state;
    this.element = document.createElement('div');
    this.element.className = 'overlays';

    this.types = document.createElement('div');
    this.weeks = document.createElement('div');
    this.types.className = 'overlays-types hidden';
    this.weeks.className = 'overlays-weeks hidden';

    types.forEach((type) => {
      const typeElement = document.createElement('button');
      typeElement.className = 'overlays-types-type';
      typeElement.setAttribute('data-type', type);
      const typeImage = document.createElement('img');
      typeImage.src = `assets/images/type-${type}.svg`;
      const typeLabel = document.createElement('p');
      typeLabel.innerText = type;
      typeElement.appendChild(typeImage);
      typeElement.appendChild(typeLabel);
      typeElement.addEventListener('click', this.changeTypes);
      this.types.appendChild(typeElement);
    });

    weeks.forEach((week, index) => {
      const weekElement = document.createElement('button');
      weekElement.className = `overlays-weeks-week ${index === 0 ? 'active' : ''}`;
      weekElement.setAttribute('data-index', index);
      weekElement.innerHTML = week;
      weekElement.addEventListener('click', this.changeWeek);
      this.weeks.appendChild(weekElement);
    });

    this.element.appendChild(this.types);
    this.element.appendChild(this.weeks);
  }

  show(element) {
    this[element].classList.remove('hidden');
  }

  hide(element) {
    this[element].classList.add('hidden');
  }

  reset() {
    this.changeWeek({ target: [...this.weeks.children][0] });
  }

  changeTypes({ target }) {
    const index = this.state.types.indexOf(target.getAttribute('data-type'));
    if (index === -1) {
      this.state.types.push(target.getAttribute('data-type'));
    } else {
      this.state.types.splice(index, 1);
    }
  }

  updateTypes(selected) {
    [...this.types.children].forEach((child) => {
      child.classList[selected.indexOf(child.getAttribute('data-type')) >= 0 ? 'add' : 'remove']('active');
    });
  }

  changeWeek({ target }) {
    const selected = parseInt(target.getAttribute('data-index'), 10);
    this.state.week = selected;
  }

  updateWeek(selected) {
    [...this.weeks.children].forEach((child, index) => {
      child.classList[index < selected ? 'add' : 'remove']('active');
      child.classList[index === selected ? 'add' : 'remove']('current');
    });
  }
}
