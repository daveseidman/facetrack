export const types = [
  'nose',
  'forehead',
  'eye',
];

export const weeks = [
  'screening',
  '<p>after</p><p>8</p></p><p>weeks</p>',
  '<p>after</p><p>16</p></p><p>weeks</p>',
  '<p>after</p><p>32</p></p><p>weeks</p>',
  '<p>after</p><p>40</p></p><p>weeks</p>',
];

export const confidenceThreshold = 0.9;
