const keys = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '←', '0', '→'];
const isLocal = window.location.href.indexOf('localhost') >= 0;

const titleText = 'Text this gif to yourself!';
const titleTextError = 'Check the number and try again';

export default class Share {
  constructor(state) {
    this.state = state;

    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.close = this.close.bind(this);
    this.loadGif = this.loadGif.bind(this);
    this.gifLoaded = this.gifLoaded.bind(this);
    this.sendGif = this.sendGif.bind(this);
    this.keypadClicked = this.keypadClicked.bind(this);

    this.phoneNumber = '';
    this.sending = false;

    this.element = document.createElement('div');
    this.element.className = 'share hidden';

    this.title = document.createElement('p');
    this.title.innerText = titleText;
    this.title.classList = 'share-title';

    this.phone = document.createElement('input');
    this.phone.className = 'share-phone';
    this.phone.type = 'text';
    this.phone.placeholder = '';

    this.sendButton = document.createElement('button');
    this.sendButton.className = 'share-send';
    this.sendButton.addEventListener('click', this.sendGif);

    this.playback = document.createElement('div');
    this.playback.className = 'share-playback';
    this.gif = document.createElement('img');
    this.gif.addEventListener('load', this.gifLoaded);

    this.keypad = document.createElement('div');
    this.keypad.className = 'share-keypad';
    for (let i = 0; i < 12; i += 1) {
      const key = document.createElement('div');
      key.className = 'share-keypad-key';
      key.innerText = keys[i];
      this.keypad.appendChild(key);
      key.addEventListener('click', this.keypadClicked);
    }

    this.closeButton = document.createElement('button');
    this.closeButton.className = 'share-close';
    this.closeButton.innerText = '×';
    this.closeButton.addEventListener('click', this.close);

    this.element.appendChild(this.title);
    this.element.appendChild(this.playback);
    this.element.appendChild(this.phone);
    this.element.appendChild(this.sendButton);
    this.element.appendChild(this.keypad);
    this.element.appendChild(this.closeButton);
  }

  show() {
    this.phone.value = '';
    this.sendButton.classList.remove('success');
    this.sendButton.classList.remove('error');
    this.element.classList.remove('hidden');
  }

  hide() {
    this.element.classList.add('hidden');
  }

  loadGif() {
    this.gif.src = `${isLocal ? 'facetrack/' : ''}images/${this.state.previewing}`;
  }

  gifLoaded() {
    this.playback.style.backgroundImage = `url('${isLocal ? 'facetrack/' : ''}images/${this.state.previewing}')`;
  }

  keypadClicked({ target }) {
    if (this.sending) return;

    this.phone.classList.remove('error');
    this.title.innerText = titleText;

    const number = parseInt(target.innerText, 10);
    if (number >= 0 && number <= 9 && this.phoneNumber.length < 10) this.phone.value += number;

    else {
      if (target.innerText === '←') {
        this.phone.value = this.phoneNumber.substring(0, this.phoneNumber.length - 1);
      }
      if (target.innerText === '→' && this.phone.value.length === 10) {
        this.sendGif();
      }
    }

    this.phoneNumber = this.phone.value.replace(/-/g, '');
    if (this.phoneNumber.length > 3) {
      if (this.phoneNumber.length <= 6) {
        this.phone.value = `${this.phoneNumber.substring(0, 3)}-${this.phoneNumber.substring(3, 6)}`;
      } else {
        this.phone.value = `${this.phoneNumber.substring(0, 3)}-${this.phoneNumber.substring(3, 6)}-${this.phoneNumber.substring(6, 10)}`;
      }
    }
    this.phone.focus();
  }

  sendGif() {
    if (this.sending) return;
    this.sendButton.classList.add('sending');
    this.sending = true;

    fetch(`${isLocal ? 'facetrack/' : ''}sendGif/+1${this.phoneNumber}/${this.state.previewing}`).then(res => res.json()).then(({ success }) => {
      this.sendButton.classList.remove('sending');
      this.sending = false;
      if (success) {
        this.sendButton.classList.add('success');
        setTimeout(() => { this.state.previewing = null; }, 1000);
      } else {
        this.phone.classList.add('error');
        this.title.innerText = titleTextError;
      }
    });
  }

  close() {
    fetch(`${isLocal ? 'facetrack/' : ''}deleteGif/${this.state.previewing}`);// .then(res => res.json()).then(({ success }) => { });
    this.state.previewing = null;
    this.playback.style.backgroundImage = 'none';
  }
}
