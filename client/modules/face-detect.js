import * as facemesh from '@tensorflow-models/facemesh';
import * as tf from '@tensorflow/tfjs-core';
import { confidenceThreshold } from './config';

export default class FaceDetect {
  constructor(state) {
    this.state = state;
    this.load = this.load.bind(this);
    this.detect = this.detect.bind(this);

    // TODO: maybe attach video reference to state instead of passing in?
    this.video = null;
    this.model = null;
  }

  load() {
    return new Promise((resolve) => {
      tf.setBackend('webgl').then(() => {
        facemesh.load({ maxFaces: 1 }).then((_model) => {
          this.model = _model;
          resolve();
        });
      });
    });
  }

  detect() {
    this.model.estimateFaces(this.video).then((predictions) => {
      this.state.faces = (predictions.length === 1 && predictions[0].faceInViewConfidence > confidenceThreshold)
        ? predictions[0].scaledMesh
        : null;
    });
    if (!this.state.previewing) requestAnimationFrame(this.detect);
  }
}
