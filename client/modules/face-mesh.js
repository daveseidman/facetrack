// TODO: apply temporal smoothing to each vertex

import { Mesh, BoxGeometry, Scene, WebGLRenderer, PerspectiveCamera, Geometry, Clock, MeshBasicMaterial, CanvasTexture } from 'three';
import FBXloader from 'three-fbxloader-offical';
import { types, weeks } from './config';
import { keypointPositionMap } from '../assets/keypoints';
import { getClosestVertex, imgLoaded } from './utils';

const keypointPositionArray = new Array(476).fill({ x: null, y: null });
keypointPositionMap.forEach(({ index, loc }) => {
  keypointPositionArray[index] = loc;
});


const verticesArray = new Array(476);
const sequenceLength = 60;
const textureSize = 1024;

export default class FaceMesh {
  constructor(state) {
    this.state = state;
    this.load = this.load.bind(this);
    this.updateMesh = this.updateMesh.bind(this);
    this.updateTypes = this.updateTypes.bind(this);
    this.faceFound = this.faceFound.bind(this);
    this.animate = this.animate.bind(this);
    this.reset = this.reset.bind(this);
    this.keypointMapping = new Array(476);

    this.scene = new Scene();
    this.camera = new PerspectiveCamera(15, 1, 1, 10000);
    this.camera.position.set(0, 0, 3600);

    this.renderer = new WebGLRenderer({ antialias: true, alpha: true });
    this.renderer.setClearColor(0x000000, 0);

    this.element = this.renderer.domElement;

    this.overlays = {};
    types.forEach((type) => {
      this.overlays[type] = { images: [], alpha: 0, on: false };
    });

    this.textureCanvas = document.createElement('canvas');
    this.textureCanvas.width = textureSize;
    this.textureCanvas.height = textureSize;
    this.textureContext = this.textureCanvas.getContext('2d');

    this.clock = new Clock();

    this.detectSequenceIndex = 0;

    this.reset();
  }

  load() {
    return new Promise((resolve) => {
      const loader = new FBXloader();
      loader.load('assets/models/facemesh.fbx', (obj) => {
        this.facemesh = obj.getObjectByName('FaceMesh');
        // these two lines create standard geometry from buffergeometry, needed because without it the amount of verts is incorrect
        // however it renumbers the verts differently if the fbx changes even slightly so we'll need to remap
        this.facemesh.geometry = new Geometry().fromBufferGeometry(this.facemesh.geometry);
        this.facemesh.geometry.mergeVertices();
        this.facemesh.geometry.dynamic = true;

        this.facemesh.geometry.vertices.forEach(({ x, y }, index) => {
          const closestVertex = getClosestVertex(x, y, keypointPositionArray);
          this.keypointMapping[closestVertex] = index;
          verticesArray[index] = closestVertex;
        });

        const loaders = [];

        Object.keys(this.overlays).forEach((key) => {
          for (let i = 0; i < sequenceLength; i += 1) {
            const img = document.createElement('img');
            img.src = `assets/textures/${key}/${key}-${i >= 10 ? i : `0${i}`}.png`;
            this.overlays[key].images.push(img);
            loaders.push(imgLoaded(img));
          }
        });

        this.detectSequence = [];
        for (let i = 0; i < 90; i += 1) {
          const img = document.createElement('img');
          img.src = `assets/textures/detected/detected-${i >= 10 ? i : `0${i}`}.png`;
          this.detectSequence.push(img);
          loaders.push(imgLoaded(img));
        }

        Promise.all(loaders).then(() => { resolve(); });

        this.canvasTexture = new CanvasTexture(this.textureCanvas);
        this.facemesh.material = new MeshBasicMaterial({ map: this.canvasTexture, transparent: true });
        this.scene.add(this.facemesh);
        this.animate();
      });
    });
  }

  start(width, height) {
    this.element.width = width;
    this.element.height = height;
    this.renderer.setSize(width, height);
    this.camera.position.set(width / 2, -height / 2, height * 3.8);
  }

  updateTypes(updatedTypes) {
    Object.keys(this.overlays).forEach((key) => {
      this.overlays[key].on = updatedTypes.indexOf(key) >= 0 ? 1 : 0;
    });
  }

  faceFound() {
    this.animating = true;
    this.detectSequenceIndex = 0;
  }

  updateMesh(scaledMesh) {
    for (let i = 0; i < scaledMesh.length; i += 1) {
      this.facemesh.geometry.vertices[this.keypointMapping[i]].x = scaledMesh[i][0];
      this.facemesh.geometry.vertices[this.keypointMapping[i]].y = -scaledMesh[i][1];
      this.facemesh.geometry.vertices[this.keypointMapping[i]].z = scaledMesh[i][2];
    }
    this.facemesh.geometry.verticesNeedUpdate = true;
    this.facemesh.geometry.computeVertexNormals();

    this.renderer.render(this.scene, this.camera);
  }

  animate() {
    this.targetFrame = (this.state.week / (weeks.length - 1)) * sequenceLength;
    this.currentFrame += (this.targetFrame - this.currentFrame) / 20;

    this.textureContext.clearRect(0, 0, textureSize, textureSize);
    Object.keys(this.overlays).forEach((key) => {
      const overlay = this.overlays[key];
      overlay.alpha += ((overlay.on ? 1 : 0) - overlay.alpha) / 20;
      this.textureContext.globalAlpha = overlay.alpha;
      this.textureContext.drawImage(overlay.images[Math.floor(this.currentFrame)], 0, 0, textureSize, textureSize);
    });

    if (this.animating) {
      this.detectSequenceIndex += 1;
      if (this.detectSequenceIndex > 88) {
        this.animating = false;
      } else {
        this.textureContext.globalAlpha = 1;
        this.textureContext.drawImage(this.detectSequence[this.detectSequenceIndex], 0, 0, textureSize, textureSize);
      }
    }

    this.canvasTexture.needsUpdate = true;

    requestAnimationFrame(this.animate);
  }

  reset() {
    this.targetFrame = 0;
    this.currentFrame = 0;
  }
}
