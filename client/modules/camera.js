export default class Camera {
  constructor() {
    this.element = document.createElement('video');
    this.element.setAttribute('muted', true);
    this.element.setAttribute('autoplay', true);
    this.element.setAttribute('playsinline', true);
  }

  load() {
    return new Promise((resolve) => {
      navigator.mediaDevices.getUserMedia({
        audio: false,
        video: {
          facingMode: 'user',
          width: 640,
          height: 480,
        },
      }).then((stream) => {
        const { width, height } = stream.getVideoTracks()[0].getConstraints();
        this.width = width;
        this.height = height;
        this.element.width = width;
        this.element.height = height;
        this.element.srcObject = stream;

        console.log(width, height);
        this.element.onloadedmetadata = () => {
          this.element.play();
          resolve();
        };
      });
    });
  }
}
