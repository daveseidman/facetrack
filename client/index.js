// TODO: more feedback during recording / share

import onChange from 'on-change';
import Camera from './modules/camera';
import FaceMesh from './modules/face-mesh';
import FaceDetect from './modules/face-detect';
import Overlays from './modules/overlays';
import Record from './modules/record';
import Share from './modules/share';

import './index.scss';

class App {
  constructor() {
    this.start = this.start.bind(this);
    this.render = this.render.bind(this);
    this.resize = this.resize.bind(this);
    this.stateChanged = this.stateChanged.bind(this);

    this.state = onChange({
      started: false,
      faces: null,
      recording: false,
      previewing: null,
      types: [],
      week: 0,
    }, this.stateChanged);

    this.element = document.createElement('div');
    this.element.className = 'app';

    this.canvas = document.createElement('canvas');
    this.canvas.className = 'canvas hidden';

    this.personOutline = document.createElement('div');
    this.personOutline.className = 'person-outline hidden';
    this.startButton = document.createElement('button');
    this.startButton.className = 'start-button';
    this.startButton.innerText = 'Start Experience';
    this.startButton.addEventListener('click', this.start);

    this.faceDetect = new FaceDetect(this.state);
    this.camera = new Camera();
    this.faceMesh = new FaceMesh(this.state);
    this.overlays = new Overlays(this.state);
    this.record = new Record(this.state);
    this.share = new Share(this.state);

    this.loading = document.createElement('div');
    this.loading.innerText = 'Loading';
    this.loading.className = 'loading ellipsis';

    this.calibrating = document.createElement('div');
    this.calibrating.innerText = 'Calibrating';
    this.calibrating.className = 'calibrating ellipsis hidden';

    this.element.appendChild(this.canvas);
    this.element.appendChild(this.personOutline);
    this.element.appendChild(this.overlays.element);
    this.element.appendChild(this.record.element);
    this.element.appendChild(this.share.element);
    this.element.appendChild(this.calibrating);
    this.element.appendChild(this.loading);

    document.body.appendChild(this.element);

    // load in the tensorflow model
    this.faceDetect.load().then(() => {
      // then get the camera while we load the threejs scene
      Promise.all([this.camera.load(), this.faceMesh.load()]).then(() => {
        this.loading.classList.add('hidden');
        this.faceDetect.video = this.camera.element;
        this.record.canvas = this.canvas;
        this.canvas.classList.remove('hidden');
        this.canvas.width = this.camera.width;
        this.canvas.height = this.camera.height;
        this.faceMesh.start(this.camera.width, this.camera.height);

        this.faceMesh.camera.aspect = this.camera.width / this.camera.height;
        this.faceMesh.camera.updateProjectionMatrix();
        this.resize();
        this.context = this.canvas.getContext('2d');
        // this.context.globalCompositeOperation = 'overlay';

        this.element.appendChild(this.startButton);

        this.render();
      });
    });

    window.addEventListener('resize', this.resize);
  }


  stateChanged(path, value, prevValue) {
    if (path !== 'faces') { // don't clutter the console with face data
      console.groupCollapsed('app state changed');
      console.log(`state changed: ${path}`);
      console.log(`previous: ${prevValue}`);
      console.log(`current: ${value}`);
      console.groupEnd();
    }

    switch (path) {
      case 'started':
        this.calibrating.classList.remove('hidden');
        this.startButton.classList.add('hidden');
        this.canvas.classList.remove('hidden');
        this.overlays.show('types');
        setTimeout(() => { this.faceDetect.detect(); }, 50);
        break;

      case 'faces':
        if (value) this.faceMesh.updateMesh(value);
        // TODO: debounce this
        if (value && !prevValue) {
          this.calibrating.classList.add('hidden');
          console.log('face found');
          this.faceMesh.faceFound();
          this.personOutline.classList.add('hidden');
        }
        if (prevValue && !value) {
          console.log('face lost');
          this.personOutline.classList.remove('hidden');
        }
        break;

      case 'types':
        this.faceMesh.updateTypes(value);
        this.overlays.updateTypes(value);
        // show or hide weeks if disease state selected
        this.overlays[value.length > 0 ? 'show' : 'hide']('weeks');
        this.record[value.length > 0 ? 'show' : 'hide']();
        // reset week to zero if no disease state selected
        // this.state.week = value.length > 0 ? this.state.week : 0;
        if (value.length === 0) this.overlays.reset();
        break;

      case 'week':
        this.overlays.updateWeek(value);
        break;

      case 'recording':
        if (value) {
          this.record.startRecording();
          this.overlays.hide('types');
          this.state.week = 0;
          this.faceMesh.reset();
        }
        break;

      case 'previewing':
        if (value) {
          this.canvas.classList.add('fade');
          this.overlays.hide('types');
          this.overlays.hide('weeks');
          this.share.show();
          this.share.loadGif();
          this.record.hide();
        } else {
          this.canvas.classList.remove('fade');
          this.overlays.show('types');
          this.overlays.show('weeks');
          this.share.hide();
          this.record.show();
          this.faceDetect.detect();
        }
        break;

      default:
        console.log(`no route matched for ${path}`);
        break;
    }
  }

  // one-time call, necessary to start videos in browser mode
  start() {
    this.state.started = true;
  }

  render() {
    // draw the camera to the main canvas
    this.context.clearRect(0, 0, this.camera.width, this.camera.height);
    this.context.drawImage(this.camera.element, 0, 0, this.camera.width, this.camera.height, 0, 0, this.camera.width, this.camera.height);

    // draw the facemesh on top of it
    if (this.state.faces) this.context.drawImage(this.faceMesh.element, 0, 0, this.camera.width, this.camera.height, 0, 0, this.camera.width, this.camera.height);
    requestAnimationFrame(this.render);
  }


  faceFound() {
    console.log('found face');
    this.personOutline.classList.add('hidden');
  }

  faceLost() {
    console.log('lost face');
    if (this.state.recording) {
      console.log('FACE LOST DURING RECORD, start over?');
    }
    this.personOutline.classList.remove('hidden');
  }

  startRecording() {
    // todo: other ui updates here
    this.canvas.classList.add('recording');
    this.record.startRecording();
  }

  resize() {
    // scale canvas up to match window height:
    const scale = window.innerHeight / this.camera.height;
    this.element.style.width = `${Math.min(scale * this.camera.width, window.innerWidth)}px`;
    this.canvas.style.transform = `scale(-${scale}, ${scale}) translate(-50%, -50%)`;
  }
}

const app = new App(); //eslint-disable-line
window.app = app;
