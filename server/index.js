require('dotenv').config();
const fs = require('fs');
const express = require('express');
const multer = require('multer');
const { spawn } = require('child_process');
const ngrok = require('ngrok');

const body = 'Thanks for sharing your face!';
const { accountSid, authToken, from } = require('../private/twilio.json');

const twilio = require('twilio')(accountSid, authToken); // eslint-disable-line

const storage = multer.diskStorage({
  destination: (req, res, cb) => { cb(null, `${__dirname}/images`); },
  filename: (req, file, cb) => cb(null, `${file.fieldname}.jpg`),
});

const upload = multer({ storage });
const app = express();

// create a local tunnel so that twilio can access images when running on localhost
let tunnelURL;
if (process.env.PRODUCTION === 'false') {
  ngrok.connect(process.env.PORT).then((res) => {
    tunnelURL = res;
    console.log('got tunnel:', tunnelURL);
  });
}

app.get('/facetrack/sendGif/:to/:hash', (req, res) => {
  const { to, hash } = req.params;
  const url = process.env.PRODUCTION === 'true'
    ? `${process.env.IMAGE_PATH}/${hash}`
    : `${tunnelURL}/facetrack/images/${hash}`;

  console.log(`texting image: ${url} to ${to}`);

  const timeout = setTimeout(() => {
    res.send({ success: false });
  }, 3000);

  twilio.messages.create({ body, from, to, mediaUrl: [url] }).then((message) => {
    console.log('sms sid:', message.sid);
    clearTimeout(timeout);
    res.send({ success: true });
  });

  // delete the gif automatically after an hour
  setTimeout(() => {
    fs.unlink(`${__dirname}/gifs/${hash}.gif`, () => { console.log('gif expired', hash); });
  }, 60 * 60 * 1000);
});

app.get('/facetrack/deleteGif/:hash', (req, res) => {
  fs.unlink(`${__dirname}/gifs/${req.params.hash}.gif`, () => {
    res.send({ success: true });
  });
});

// TODO rename this, too close to sendGif
app.post('/facetrack/sendImage/:hash', upload.any(), (req, res) => {
  console.log('receiving images', req.params.hash);
  const convertToGif = spawn(process.env.PYTHON, ['server/scripts/create-gif.py', req.params.hash]);
  convertToGif.stdout.on('data', (data) => {
    console.log(`data:${data}`);
  });
  convertToGif.stderr.on('data', (data) => {
    console.log(`error:${data}`);
    res.send({ success: false });
  });
  convertToGif.stderr.on('close', () => {
    console.log('created gif successfully');
    res.send({ success: true });
  });
});

// access gifs
app.get('/facetrack/images/:hash', (req, res) => {
  console.log(req.params.hash);
  res.sendFile(`${__dirname}/gifs/${req.params.hash}.gif`);
});


app.use('/facetrack', express.static('dist'));

app.listen(process.env.PORT, () => { console.log(`app listening on port ${process.env.PORT}`); });
