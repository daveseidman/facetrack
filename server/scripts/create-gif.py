import os
import sys
import imageio
from PIL import Image

images = []

hash = sys.argv[1];

files = [
    "server/images/" + hash + "-1.jpg",
    "server/images/" + hash + "-2.jpg",
    "server/images/" + hash + "-3.jpg",
    "server/images/" + hash + "-4.jpg",
    "server/images/" + hash + "-5.jpg"
]

for file in files:
    im = Image.open(file)
    im.thumbnail([320, 240], Image.ANTIALIAS)
    crop = im.crop([40, 0, 280, 240])
    flip = crop.transpose(Image.FLIP_LEFT_RIGHT)
    rgb = flip.convert("RGB")
    rgb.save(file, "JPEG")

for file in files:
    images.append(imageio.imread(file))
    
imageio.mimsave("server/gifs/" + hash + ".gif", images, 'GIF', fps=5, subrectangles=True, palettesize=256)

for file in files:
    os.remove(file)
